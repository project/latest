<?php
  // Check whether the tags vocabulary exists (created as part of the standard install profile)
  $taxonomy = taxonomy_vocabulary_get_names();
  if (!isset($taxonomy['tags'])) {
    // Create a taxonomy vocabulary named "Tags"
    $description = st('Use tags to group articles on similar topics into categories.');
    $help = st('Enter a comma-separated list of words to describe your content.');
    $vocabulary = (object) array(
      'name' => st('Tags'), 
      'description' => $description, 
      'machine_name' => 'tags', 
      'help' => $help,
    );
    taxonomy_vocabulary_save($vocabulary);
  }

  // Check whether the tags field exists (created as part of the standard install profile)
  if (field_info_field('field_tags') == NULL) {
    $field = array(
      'field_name' => 'field_tags', 
      'type' => 'taxonomy_term_reference',
      // Set cardinality to unlimited for tagging. 
      'cardinality' => FIELD_CARDINALITY_UNLIMITED, 
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name, 
            'parent' => 0,
          ),
        ),
      ),
    );
    field_create_field($field);
  }

  $fields = array(
    // channel - used on latest_story
    'source_channel' => array(
      'field_name' => 'field_latest_channel',
      'type' => 'entityreference',
      'cardinality' => 1,
      'settings' => array(
        'target_type' => 'node',
        'handler_settings' => array(
          'target_bundles' => array('latest_channel'),
        ),
      ),
    ),
    // source - used on latest_story
    'source' => array(
      'field_name' => 'field_latest_source',
      'type' => 'link_field',
      'entity_types' => array('node'),
    ),
  );

  foreach ($fields as $field) {
    if (!field_info_field($field['field_name'])) {
      field_create_field($field);
    }
  }
